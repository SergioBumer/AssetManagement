package exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class AssetNotFoundedException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7780637293222660886L;

	/**
	 * 
	 */
	

	public AssetNotFoundedException(String mensaje) {
		super(mensaje);
	}
}
