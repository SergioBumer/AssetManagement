package exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FechaBajaException extends RuntimeException{
	private static final long serialVersionUID = 7780637293222660886L;

	public FechaBajaException(String mensaje) {
		super(mensaje);
	}
	
}
