package exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PUTAssetNotFoundedException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8513694388326373914L;
	public PUTAssetNotFoundedException(String message) {
		super(message);
	}

}
