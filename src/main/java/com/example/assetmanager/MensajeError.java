package com.example.assetmanager;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MensajeError {
	private String mensaje;
	private int codigo;
	private int extraInfo;
	public MensajeError(String mensaje, int codigo, int extraInfo) {
		
		this.mensaje = mensaje;
		this.codigo = codigo;
		this.extraInfo = extraInfo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getExtraInfo() {
		return extraInfo;
	}
	public void setExtraInfo(int extraInfo) {
		this.extraInfo = extraInfo;
	}
	@Override
	public String toString() {
		return "MensajeError [mensaje=" + mensaje + ", codigo=" + codigo + ", extraInfo=" + extraInfo + "]";
	}
	
	
}
