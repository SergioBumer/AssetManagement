package com.example.assetmanager;

import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import exceptions.PUTAssetNotFoundedException;
import exceptions.UnvalidDateException;
import exceptions.AssetNotFoundedException;
import exceptions.FechaBajaException;
import exceptions.IdNotIngressedException;

@RestController
@RequestMapping("/asset")
public class AssetController {
	@Autowired
	private AssetRepository assetRep;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody List<ActivoF> getAllAsset() {
		List<ActivoF> lista=assetRep.findAll();
		if (lista.size()==0) {
			throw new AssetNotFoundedException("No se encuentran activos");
		}
		else {
			return lista;
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ActivoF createAsset(@Valid @RequestBody ActivoF asset) {
		
		asset.setId(ObjectId.get());
		if(!asset.getPersona().equals("")) {
			asset.setState("Asignado");
		}
		else {
			asset.setState("Disponible");
		}
		assetRep.save(asset);
		return asset;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody ActivoF modifyAssetById(@PathVariable("id") ObjectId id, @Valid @RequestBody ActivoF asset) {
		
		ActivoF assetMod;
		Date fechaCompra;
		Date fechabaja;
		if(id==null) {
			throw new IdNotIngressedException("No se ingresó el ID del Activo");
		}
		else {
			
			assetMod = assetRep.findByid(id);
			if(assetMod!=null) {
				try {
					fechabaja = asset.getFechaBaja();
					fechaCompra = assetMod.getFechaCompra();
				}catch(UnvalidDateException e) {
					throw new UnvalidDateException("El formato de la fecha no es válido.");
				}
				
				
				if (!asset.getFechaBaja().equals(null) || !asset.getSerial().equals(null)) {
					if (!asset.getSerial().equals(null)) {
						assetMod.setSerial(asset.getSerial());
					}
					if (!asset.getFechaBaja().equals(null)) {
						if (fechaCompra.before(fechabaja)) {
							assetMod.setState("Dado_de_baja");
							assetMod.setFechaBaja(asset.getFechaBaja());
						} else {
							throw new FechaBajaException("La fecha de baja no es valida. Ingrese una fecha posterior a la fecha de compra");
						}
					}
					assetRep.save(assetMod);
				}
			}
			else {
				throw new PUTAssetNotFoundedException("El id ingresado no corresponde a ningun activo en la base de datos.");
			}
			
		}
		

		return assetMod;
	}
}