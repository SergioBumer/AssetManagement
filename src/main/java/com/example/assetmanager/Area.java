package com.example.assetmanager;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Area {

	@Id
	private ObjectId _id;
	private String nombre;
	private List<Ciudad> ciudades;
	public Area() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Area(ObjectId _id, String nombre, List<Ciudad> ciudades) {
		this._id = _id;
		this.nombre = nombre;
		this.ciudades = ciudades;
	}


	public ObjectId get_id() {
		return _id;
	}

	public String getNombre() {
		return nombre;
	}

	public List<Ciudad> getCiudades() {
		return ciudades;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setCiudades(List<Ciudad> ciudades) {
		this.ciudades = ciudades;
	}

	@Override
	public String toString() {
		return "Area [_id=" + _id + ", nombre=" + nombre + ", ciudades=" + ciudades + "]";
	}
	
}
