package com.example.assetmanager;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class AssetmanagerApplication extends SpringBootServletInitializer{

	//Despligue con archivo JAR
	public static void main(String[] args) {
		SpringApplication.run(AssetmanagerApplication.class, args);
	}
	//Despligue con archivo WAR
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(AssetmanagerApplication.class);
	}
	
	
	
}
