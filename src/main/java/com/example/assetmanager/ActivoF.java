package com.example.assetmanager;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import exceptions.UnvalidDateException;

public class ActivoF {
	@Id
	private ObjectId id;
	private String nombre;
	private String descripcion;
	private String tipo;
	private String serial;
	private int numeroInv;
	@Min(value = 0,message="El valor del peso debe ser mayor a 0")
	private double peso;
	@Min(value = 0,message="El valor del alto debe ser mayor a 0")
	private double alto;
	@Min(value = 0,message="El valor del ancho debe ser mayor a 0")
	private double ancho;
	@Min(value = 0,message="El valor del largo debe ser mayor a 0")
	private double largo;
	@Min(value = 0,message="El valor de la compra debe ser mayor a 0")
	private double valorCompra;
	private String persona;
	@DateTimeFormat(pattern="yyyy/MM/dd")
	private Date fechaCompra;
	@DateTimeFormat(pattern="yyyy/MM/dd")
	private Date fechaBaja;
    enum Estado {
		
		Activo("ACTIVO"),Disponible("DISPONIBLE"),Dado_de_baja("DADO_DE_BAJA"),Reparacion("REPARACIÓN"),Asignado("ASIGNADO");
		private Estado(String est) {
			this.est=est;
		}
		public String getEstado() {
			return est;
		}
		private String est;
	}
    private Estado state;
    
	private String color;
	public ActivoF(ObjectId id, String nombre, String descripcion, String tipo, String serial, int numeroInv,
			double peso, double alto, double ancho, double largo, double valorCompra, Date fechaCompra, Date fechaBaja,
			String estadoEnt,String color,String persona) {
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tipo = tipo;
		this.serial = serial;
		this.numeroInv = numeroInv;
		this.peso = peso;
		this.alto = alto;
		this.ancho = ancho;
		this.largo = largo;
		this.valorCompra = valorCompra;
		try {
			this.fechaCompra = fechaCompra;
		}catch(UnvalidDateException e) {
			throw new UnvalidDateException("El formato de la fecha ingresada no es válido");
		}
		this.persona=persona;
		this.fechaBaja = fechaBaja;
		this.state=Enum.valueOf(Estado.class, estadoEnt);
		this.color = color;
	}
	public ActivoF() {
		
	}
	
	public String getSerial() {
		return serial;
	}
	public Date getFechaCompra() {
		return fechaCompra;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public Estado getState() {
		return state;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public void setFechaCompra(Date fechaCompra) {
		try {
			this.fechaCompra = fechaCompra;
		}catch(UnvalidDateException e) {
			throw new UnvalidDateException("El formato de la fecha ingresada no es válido");
		}
		
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public void setState(Estado state) {
		this.state = state;
	}
	public void setState(String value) {
		this.state = Enum.valueOf(Estado.class, value);
	}
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public String getPersona() {
		return persona;
	}
	
	public String getNombre() {
		return nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public String getTipo() {
		return tipo;
	}
	public int getNumeroInv() {
		return numeroInv;
	}
	public double getPeso() {
		return peso;
	}
	public double getAlto() {
		return alto;
	}
	public double getAncho() {
		return ancho;
	}
	public double getLargo() {
		return largo;
	}
	public double getValorCompra() {
		return valorCompra;
	}
	public String getColor() {
		return color;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setNumeroInv(int numeroInv) {
		this.numeroInv = numeroInv;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public void setAlto(double alto) {
		this.alto = alto;
	}
	public void setAncho(double ancho) {
		this.ancho = ancho;
	}
	public void setLargo(double largo) {
		this.largo = largo;
	}
	public void setValorCompra(double valorCompra) {
		this.valorCompra = valorCompra;
	}
	public void setPersona(String persona) {
		this.persona = persona;
	}
	public void setColor(String color) {
		this.color = color;
	}
	@Override
	public String toString() {
		return "ActivoF [_id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", tipo=" + tipo
				+ ", serial=" + serial + ", numeroInv=" + numeroInv + ", peso=" + peso + ", alto=" + alto + ", ancho="
				+ ancho + ", largo=" + largo + ", valorCompra=" + valorCompra + ", persona=" + persona
				+ ", fechaCompra=" + fechaCompra + ", fechaBaja=" + fechaBaja + ", state=" + state + ", color=" + color
				+ "]";
	}
	
	
	
	
	
}
