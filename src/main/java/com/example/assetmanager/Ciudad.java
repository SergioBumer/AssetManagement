package com.example.assetmanager;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Ciudad {

	@Id
	private ObjectId _id;
	private String nombre;
	private List<Area> area;
	
	public Ciudad() {
		// TODO Auto-generated constructor stub
	}

	public Ciudad(ObjectId _id, String nombre, List<Area> area) {
		this._id = _id;
		this.nombre = nombre;
		this.area = area;
	}

	public ObjectId get_id() {
		return _id;
	}

	public String getNombre() {
		return nombre;
	}

	public List<Area> getArea() {
		return area;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setArea(List<Area> area) {
		this.area = area;
	}

	@Override
	public String toString() {
		return "Ciudad [_id=" + _id + ", nombre=" + nombre + ", area=" + area + "]";
	}
	
}
