package com.example.assetmanager;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
public interface AssetRepository extends MongoRepository<ActivoF, String>{
	ActivoF findByid(ObjectId _id);
}
